public class HotelNumber {
    private float cost;
    private int square;

    public float attractiveness(){
        return cost/square;
    }

    public float getCost(){
        return cost;
    }

    public void Init(float newCost, int newSquare){
        cost = newCost;
        square = newSquare;
    }

    public void Display(){
        System.out.print("Стоимость номера: " + cost + '\n');
        System.out.print("Площать номера: " + square + '\n');
    }
}
