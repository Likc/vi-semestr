public class Hotel {
    HotelNumber num1 = new HotelNumber();
    HotelNumber num2 = new HotelNumber();
    HotelNumber num3 = new HotelNumber();
    int lasting;
    String name = "Имя отеля";

    public void Init()
    {
        num1.Init(1000,20);
        num2.Init(1500,20);
        num3.Init(1200,21);
        lasting = 93;
    }
    public float profit(){
        return (num1.getCost()+num2.getCost()+num3.getCost())*lasting;
    }

    public void theBest(){
        float best = num1.attractiveness();
        if (best<num2.attractiveness()) {
            best = num2.attractiveness();
        }
        if (best<num3.attractiveness()){
            best=num3.attractiveness();
        }
        System.out.print("Лучший номер имеет привлекательность: " + best + '\n');
    }
}
