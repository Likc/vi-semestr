import java.util.Random;

public class Lab_1 {
    public static void main(String args[]) {
        Vect Vect1 = new Vect();
        Vect Vect2 = new Vect();
        Vect1.Init(1, 2);//init
        Vect2.Read();//read
        System.out.print("Длина первого вектора " + Vect1.Length() + '\n');//пример с длиной вектора
        Vect1.Sum(Vect1, Vect2).Display();
        Vect1.Sum(Vect2).Display();


        Random rnd = new Random();
        float SumOfLen = 0;
        Vect MassVect[] = new Vect[3];
        for (Vect i :
                MassVect) {
            i = new Vect(rnd.nextInt(10), rnd.nextInt(10));
            SumOfLen += i.Length();
            System.out.print("Длина вектора из массива = " + i.Length() + '\n');
        }
        System.out.print("Сумма длин = " + SumOfLen + '\n');
    }
}
