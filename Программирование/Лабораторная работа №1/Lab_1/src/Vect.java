import java.util.*;

public class Vect {
    private int x;
    private int y;

    public Vect(){
        x=0;
        y=0;
    }
    public Vect(int NewX, int NewY){
        x=NewX;
        y=NewY;
    }
    public void Init(int newX, int newY) {
        x = newX;
        y = newY;
    }

    public void Read() {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите X:\n");
        x = in.nextInt();
        System.out.print("Введите Y:\n");
        y = in.nextInt();
    }

    public void Display() {
        System.out.print("Вектор с координатами \nX: " + x + '\n');
        System.out.print("Y: " + y + '\n');
    }

    public float Length() {
        return (float) Math.sqrt(x ^ 2 + y ^ 2);
    }

    public Vect Sum(Vect FirstVect, Vect SecondVect) {
        Vect ResVect = new Vect(FirstVect.x + SecondVect.x,FirstVect.y+SecondVect.y);
        return ResVect;
    }

    public Vect Sum( Vect SecondVect) {
        Vect ResVect = new Vect(this.x + SecondVect.x,this.y+SecondVect.y);
        return ResVect;
    }
}
