import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class Form {
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton button1;
    private JPanel Panel;
    private JLabel res;
    private JButton выходButton;

    public void calculate(JFrame form) {
        try {
            double x = Double.parseDouble(textField1.getText());
            double y = Double.parseDouble(textField2.getText());
            double z = Double.parseDouble(textField3.getText());
            double V = x * y * z;
            double S = Math.sqrt(x * x + y * y + z * z);
            String resStr = String.format("Объем = %.3f Площадь = %.3f", V, S);
            res.setText(resStr);
//                res.setText("<html><div align=\"center\"> Объем = " + V + "<br> Площадь = " + S + "</div></html>");
            form.pack();
        } catch (Exception e) {

        }
    }

    public Form(JFrame form) {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculate(form);

            }
        });
        textField1.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                if (textField1.getText().compareTo("") != 0) {
                    try {
                        Double.parseDouble(textField1.getText());
                    } catch (Exception ex) {
                        textField1.requestFocus();
//                    e.
                        return;
                    }
                    super.focusLost(e);
                    calculate(form);
                }
            }
        });
        textField2.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                if (textField2.getText().compareTo("") != 0) {
                    try {
                        Double.parseDouble(textField2.getText());
                    } catch (Exception ex) {
                        textField2.requestFocus();
                        return;
                    }
                    super.focusLost(e);
                    calculate(form);
                }
            }
        });
        textField3.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                if (textField3.getText().compareTo("") != 0) {
                    try {
                        Double.parseDouble(textField3.getText());
                    } catch (Exception ex) {
                        textField3.requestFocus();
                        return;
                    }
                    super.focusLost(e);
                    calculate(form);
                }
            }
        });
        выходButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
    }

    public static void main(String[] args) {
        JFrame form = new JFrame("Form");
        form.setContentPane(new Form(form).Panel);
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.pack();
        form.setVisible(true);
    }
}
